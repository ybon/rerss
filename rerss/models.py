from datetime import datetime

import requests_async as requests
import feedparser
import peewee

db = peewee.SqliteDatabase("rerss.db")


class Feed(peewee.Model):

    title = peewee.CharField(max_length=256)
    subtitle = peewee.TextField()
    link = peewee.CharField()
    updated = peewee.DateTimeField()
    synced = peewee.DateTimeField()
    active = peewee.BooleanField(default=True)

    class Meta:
        database = db

    def __str__(self):
        return self.title or ""

    def __repr__(self):
        return '<Feed "{}">'.format(str(self))

    @property
    def json(self):
        return {
            "title": self.title,
            "subtitle": self.subtitle,
            "link": self.link,
            "active": self.active,
        }

    async def sync(self):
        resp = await requests.get(self.link)
        data = feedparser.parse(resp.text)
        feed = data["feed"]
        if not self.title:
            self.title = feed.get("title", "missing title fixme")
            self.subtitle = feed.get("subtitle", "missing subtitle fixme")
        updated = data.get("updated_parsed", feed.get("updated_parsed"))
        if not updated:
            updated = datetime.now()
        else:
            updated = datetime(*updated[:6])
        self.updated = updated
        self.synced = datetime.now()
        self.save()
        for entry in data["entries"]:
            Entry.attach(self, **entry)

    @classmethod
    async def from_link(cls, link):
        feed = Feed.select().where(Feed.link == link).first()
        if not feed:
            feed = cls(link=link)
        await feed.sync()
        return feed


class Entry(peewee.Model):

    title = peewee.CharField(max_length=512)
    summary = peewee.TextField()
    content = peewee.TextField(null=True)
    link = peewee.CharField()
    image = peewee.CharField(null=True)
    updated = peewee.DateTimeField()
    flagged = peewee.BooleanField(default=False, index=True)
    seen = peewee.BooleanField(default=False, index=True)
    bookmark = peewee.BooleanField(default=False, index=True)
    sources = peewee.ManyToManyField(Feed)

    class Meta:
        order_by = ("-updated",)
        database = db

    def __str__(self):
        return self.title or ""

    def __repr__(self):
        return '<Entry "{}">'.format(str(self)[:50])

    @property
    def json(self):
        return {
            "id": self.id,
            "title": self.title,
            "summary": self.summary,
            "content": self.content,
            "flagged": self.flagged,
            "seen": self.seen,
            "bookmark": self.bookmark,
            "image": self.image,
            "link": self.link,
            "updated": self.updated,
            "sources": [feed.json for feed in self.sources],
        }

    @classmethod
    def attach(cls, feed, **data):
        entry = Entry.select().where(Entry.link == data["link"]).first()
        if not entry:
            updated = data.get("updated_parsed", data.get("published_parsed"))
            if not updated:
                updated = feed.updated
            else:
                updated = datetime(*updated[:6])
            image = None
            for link in data["links"]:
                if link.get("type").startswith("image"):
                    image = link["href"]
            summary = data.get("summary", data.get("description", ""))
            entry = cls.create(
                title=data["title"],
                link=data["link"],
                summary=summary,
                updated=updated,
                image=image,
            )
        if feed not in entry.sources:
            entry.sources.add([feed])
