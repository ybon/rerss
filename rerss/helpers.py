import re

import requests_async as requests
from cssselect import HTMLTranslator
from lxml.html import fromstring, tostring
from lxml.html.clean import clean_html

# TODO: make me extendable.
# We don't attach this to the feed itself, because in ReRSS world a RSS
# can contain any origin source.
SELECTORS = {r"^https?://(www.)?lemonde.fr": "#articleBody"}

SELECTORS = {
    re.compile(p): HTMLTranslator().css_to_xpath(s) for p, s in SELECTORS.items()
}


async def fetch_content(link):
    for pattern, selector in SELECTORS.items():
        if pattern.match(link):
            resp = await requests.get(link)
            doc = fromstring(resp.text)
            try:
                node = doc.xpath(selector)[0]
            except IndexError:
                return
            return clean_html(tostring(node))
