from minicli import cli, run
from roll.extensions import logger, simple_server, static

from rerss.app import app
from rerss.models import Entry, Feed, db


@cli
def initdb():
    db.create_tables([Feed, Entry, Entry.sources.get_through_model()], safe=True)


@cli
def serve(log=False, reload=False):
    if log:
        logger(app)
    if reload:
        import hupper

        hupper.start_reloader("rerss.cli.serve")
    static(app, prefix="/app", root="rerss/app")
    simple_server(app, port=1707, host="0.0.0.0")


@cli
async def sync():
    for feed in Feed.select().where(Feed.active == True):
        print("Syncing", feed)
        await feed.sync()
        yield feed
        print("Done", feed)


if __name__ == "__main__":
    run()
