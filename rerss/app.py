from datetime import datetime, timezone
from urllib.parse import unquote

import ujson as json
import xmltodict
from roll import Roll
from roll.extensions import cors, options, traceback

from .models import Entry, Feed

# Idea add extracts

app = Roll()
cors(app, headers=["content-type"])
options(app)
traceback(app)


@app.route("/sync", protocol="websocket")
async def sync(req, ws):
    from .cli import sync

    total = Feed.select().where(Feed.active == True).count()
    done = 0
    async for feed in sync():
        done += 1
        await ws.send(json.dumps({"progress": done / total}))
    await ws.send(json.dumps({"status": "done"}))


@app.route("/feed")
async def feeds(req, resp):
    resp.json = {"feeds": [feed.json for feed in Feed.select().order_by(Feed.title)]}


@app.route("/feed", methods=["POST"])
async def new_feed(req, resp):
    payload = json.loads(req.body.decode())
    feed = await Feed.from_link(payload["link"])
    resp.json = feed.json


@app.route("/feed/{link:path}", methods=["GET", "POST"])
async def feed(req, resp, link):
    feed = Feed.get(Feed.link == unquote(link))
    if req.method == "POST":
        payload = json.loads(req.body.decode())
        for key, value in payload.items():
            setattr(feed, key, value)
        feed.save()
    resp.json = feed.json


@app.route("/entry")
async def entries(req, resp):
    limit = req.query.int("limit", 100)
    offset = req.query.int("offset", 0)
    qs = Entry.select()
    seen = req.query.bool("seen", None)
    if seen is not None:
        qs = qs.filter(Entry.seen == seen)
    bookmark = req.query.bool("bookmark", None)
    if bookmark is not None:
        qs = qs.filter(Entry.bookmark == bookmark)
    qs = qs.order_by(Entry.updated.desc()).offset(offset).limit(limit)
    resp.json = {"entries": [entry.json for entry in qs]}


@app.route("/entry/{link:path}")
async def entry(req, resp, link):
    link = unquote(link)
    entry = Entry.get(Entry.link == link)
    resp.json = entry.json


@app.route("/entry/{link:path}", methods=["POST"])
async def update_entry(req, resp, link):
    link = unquote(link)
    payload = json.loads(req.body.decode())
    payload.pop("sources", None)  # M2M.
    Entry.update(**payload).where(Entry.link == link).execute()
    entry = Entry.get(Entry.link == link)
    resp.json = entry.json


@app.route("/myfeed/atom")
async def myfeed(req, resp):
    feed = {
        "feed": {
            "@xmlns": "http://www.w3.org/2005/Atom",
            "title": "ReRss",
            "link": req.path,
            "updated": datetime.now(timezone.utc),  # Fixme.
            "entry": [],
        }
    }
    entries = (
        Entry.select()
        .where(Entry.flagged == True)
        .order_by(Entry.updated.desc())
        .limit(15)
    )
    for entry in entries:
        feed["feed"]["entry"].append(
            {
                "title": entry.title,
                "summary": entry.summary,
                "updated": entry.updated,
                "link": {"@href": entry.link},
            }
        )
    resp.body = xmltodict.unparse(feed)
    resp.headers["Content-Type"] = "application/xml"
