# ReRSS Python server


## Install

- create a venv:

    python -m venv path/to/venv

- activate it:

    source path/to/venv/bin/activate

- install

    python setup.py develop


## Run locally

Make sure your venv is activated, then:

    rerss initdb
    rerss serve

Optionally logs the requests:

    rerss serve --log

Or autoreload the server when you change a python file (needs `hupper` to be
installed):

    rerss serve --reload

Then browse to http://localhost:1707/app/index.html
